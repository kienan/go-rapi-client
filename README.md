## Testing

The test target may be set using environment variables:

```
export RAPI_USERNAME=example
export RAPI_PASSWORD=asdf
export RAPI_HOST=ganeti.example.com
```

Other available environment variables:

* `RAPI_PORT`: The target port (default: 5080)
* `RAPI_VERSION`: The API version (default: 2)
* `RAPI_TLS_VERIFY`: Should the certificate verification be enabled (default: false)

Once the environment is configured, tests may be run using `go test`
