package rapi

import (
	"os"
	"strconv"
	"testing"
)

func getTestClient(t *testing.T) *Client {
	username := os.Getenv("RAPI_USERNAME")
	if len(username) == 0 {
		username = "test"
	}
	password := os.Getenv("RAPI_PASSWORD")
	if len(password) == 0 {
		password = "test"
	}
	host := os.Getenv("RAPI_HOST")
	if len(host) == 0 {
		host = "ganeti.burntworld.test"
	}
	port := os.Getenv("RAPI_PORT")
	if len(port) == 0 {
		port = "5080"
	}
	version := os.Getenv("RAPI_VERSION")
	if len(version) == 0 {
		version = "2"
	}
	_port, port_error := strconv.Atoi(port)
	if port_error != nil {
		t.Errorf("Failed to convert client port '%v' to integer: %v", port, port_error)
	}
	_version, version_error := strconv.Atoi(version)
	if version_error != nil {
		t.Errorf("Failed to convert '%v' to integer: %v", version, version_error)
	}
	tlsverify := os.Getenv("RAPI_TLS_VERIFY")
	if len(tlsverify) == 0 {
		tlsverify = "false"
	}
	_tlsverify, tlsverify_err := strconv.ParseBool(tlsverify)
	if tlsverify_err != nil {
		t.Errorf("Failed to convert '%v' to bool: %v'", tlsverify, tlsverify_err)
	}
	return NewClient(username, password, host, _port, _version, _tlsverify)
}

func TestClusterInfo(t *testing.T) {
	c := getTestClient(t)
	_, err := c.GetClusterInformation()
	if err != nil {
		t.Errorf("Got error requesting cluster info %s", err)
	}
}

func TestGetNetworks(t *testing.T) {
	c := getTestClient(t)
	_, err := c.GetNetworks()
	if err != nil {
		t.Errorf("Got error requesting network list %s", err)
	}
}

func TestGetNetworksBulk(t *testing.T) {
	c := getTestClient(t)
	_, err := c.GetNetworksBulk()
	if err != nil {
		t.Errorf("Got error requesting network list %s", err)
	}
}

func TestGetNetworkInformation(t *testing.T) {
	c := getTestClient(t)
	_, err := c.GetNetworks()
	if err != nil {
		t.Errorf("Got error requesting network list %s", err)
	}
}
